package edu.buet.cse.ch02.v10.ejb;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

public class InterestRate implements Serializable {
  private BigDecimal value;
  
  public InterestRate() {
    this(BigDecimal.ZERO);
  }
  
  public InterestRate(BigDecimal value) {
    this.value = Objects.requireNonNull(value);
  }

  public BigDecimal getValue() {
    return value;
  }

  public void setValue(BigDecimal value) {
    this.value = value;
  }
}
