package edu.buet.cse.ch02.v10.ejb;

import java.math.BigDecimal;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

@Stateless
@LocalBean
public class RateCalculatorBean {

  public void calculateNewRate(InterestRate rate) {
    rate.setValue(BigDecimal.valueOf(2.5));

    System.out.println("interest rate was updated successfully...");
  }
}
