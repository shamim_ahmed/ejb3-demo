package edu.buet.cse.ch02.v10.web;

import edu.buet.cse.ch02.v10.ejb.RateCalculatorBean;
import edu.buet.cse.ch02.v10.ejb.InterestRate;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.annotation.WebServlet;

import javax.ejb.EJB;

@WebServlet(urlPatterns = "/invoke-calculator")
public class BeanInvokerServlet extends HttpServlet {

  @EJB
  private RateCalculatorBean rateCalculator;

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    
    InterestRate rate = new InterestRate(BigDecimal.TEN);
    BigDecimal initialValue = rate.getValue();
    
    rateCalculator.calculateNewRate(rate);
    BigDecimal finalValue = rate.getValue();

    out.println("<html>");
    out.println("<head>");
    out.println("<title>Name List</title>");
    out.println("</head>");
    out.println("<body>");
    out.println("<p>");
    out.println("Before bean invocation, rate = " + initialValue);
    out.println("<br/>");
    out.printf("After bean invocation, rate = " + finalValue);
    out.println("</p>");
    out.println("</body>");
    out.println("</html>");

    out.flush();
  }
}
