package edu.buet.cse.ch01.v10.ejb;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;

@Stateless
@LocalBean
public class SalutationBean {

  @PostConstruct
  public void init() {
    System.out.println("SalutationBean initialized");
  }

  public String getFormalSalutation(String name) {
    return String.format("Dear %s", name);
  }

  public String getInformalSalutation(String name) {
    return String.format("Hi %s", name);
  }

  @PreDestroy
  public void cleanup() {
    System.out.println("SalutationBean is being destroyed...");
  }
}
