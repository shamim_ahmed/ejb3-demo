package edu.buet.cse.ch02.v2.ejb;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.LocalBean;
import javax.ejb.PostActivate;
import javax.ejb.PrePassivate;
import javax.ejb.Stateful;

@Stateful
@LocalBean
public class NameProcessorBean {

  private List<String> nameList;

  @PostConstruct
  private void init() {
    nameList = new ArrayList<>();
  }

  public void addName(String name) {
    nameList.add(name);
  }

  public List<String> getNames() {
    return Collections.unmodifiableList(nameList);
  }
  
  @PrePassivate
  private void saveState() {
    System.out.println("NameProcessorBean is being prepared for passivation...");
  }
  
  @PostActivate
  private void restoreState() {
    System.out.println("NameProcessorBean has been restored...");
  }

  @PreDestroy
  private void cleanUp() {
    System.out.println("NameProcessorBean is being removed...");
  }
}
