package edu.buet.cse.ch02.v4.web;

import edu.buet.cse.ch02.v4.ejb.GameBean;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.annotation.WebServlet;

import javax.ejb.EJB;
import org.apache.commons.lang3.StringUtils;

@WebServlet(urlPatterns = "/game-info")
public class GameInfoServlet extends HttpServlet {
  private static final String PARAM_NAME = "title";
  
  @EJB
  private GameBean game;

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    out.println("<html>");
    out.println("<head>");
    out.println("<title>Game Info</title>");
    out.println("</head>");
    out.println("<body>");
    out.println("<p>");
    out.println(String.format("Game title: %s", game.getTitle()));
    out.println("</p>");
    out.println("</body>");
    out.println("</html>");

    out.flush();
  }
  
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String title = request.getParameter(PARAM_NAME);
    String result;
    
    if (StringUtils.isNotBlank(title)) {
      game.setTitle(title); 
      result = "Game title has been updated successfully";
    } else {
      result = "No valid title has been provided";
    }
    
    response.setContentType("text/html");
    
    PrintWriter out = response.getWriter();
    out.println("<html>");
    out.println("<head>");
    out.println("<title>Result</title>");
    out.println("</head>");
    out.println("<body>");
    out.println("<p>");
    out.println(result);
    out.println("</p>");
    out.println("</body>");
    out.println("</html>");
    
    out.flush();
  }
}
