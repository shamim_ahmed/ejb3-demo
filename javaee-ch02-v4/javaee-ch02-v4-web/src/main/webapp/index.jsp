<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Home Page</title>
  </head>
  <body>
    <h1>Simple example of Singleton Session Bean</h1>
    
    <form action="${pageContext.servletContext.contextPath}/game-info" method="POST">
      <input type="text" name="title" />
      <input type="submit" value="Submit"/>
    </form>
  </body>
</html>
