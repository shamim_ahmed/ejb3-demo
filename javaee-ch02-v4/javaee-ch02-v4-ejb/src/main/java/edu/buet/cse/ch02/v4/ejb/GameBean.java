package edu.buet.cse.ch02.v4.ejb;

import javax.annotation.PostConstruct;
import javax.ejb.DependsOn;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;

@Singleton
@LocalBean
@DependsOn("MyFavoritePlayerBean")
public class GameBean {

  private String title;

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  @PostConstruct
  private void init() {
    title = "The Armageddon";
    System.out.printf("GameBean has been initialized with title %s%n", title);
  }
}
