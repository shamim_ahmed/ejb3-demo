package edu.buet.cse.ch01.v7.web;

import java.io.IOException;
import java.io.PrintWriter;
import javax.annotation.Resource;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.annotation.WebServlet;
import org.apache.commons.lang3.StringUtils;

@WebServlet(urlPatterns = "/greeting")
public class GreetingServlet extends HttpServlet {

  private static final String NAME_PARAM_NAME = "name";
  private static final String DEFAULT_NAME = "Duke";

  @Resource(mappedName = "jms/salutationConnectionFactory")
  private QueueConnectionFactory connectionFactory;

  @Resource(mappedName = "jms/salutationQueue")
  private Queue queue;

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    response.setContentType("text/html");

    String name = request.getParameter(NAME_PARAM_NAME);

    if (StringUtils.isBlank(name)) {
      name = DEFAULT_NAME;
    }

    String resultStr = null;

    try {
      QueueConnection connection = connectionFactory.createQueueConnection();
      Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
      MessageProducer producer = session.createProducer(queue);

      TextMessage message = session.createTextMessage();
      message.setStringProperty("name", name);
      producer.send(message);
      resultStr = "A message has been sent to JMS Queue";
    } catch (JMSException ex) {
      resultStr = "An error occurred while sending a message to JMS Queue";
      System.err.println(ex);
    }

    PrintWriter out = response.getWriter();
    out.println("<html>");
    out.println("<head>");
    out.println("<title>Salutation Service</title>");
    out.println("</head>");
    out.println("<body>");
    out.println(String.format("<p>%s</p>", resultStr));
    out.println("<p>Please check server log</p>");
    out.println("</body>");
    out.println("</html>");

    out.flush();
  }
}
