package edu.buet.cse.ch01.v7.ejb;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;

@MessageDriven(mappedName = "jms/salutationQueue",
    activationConfig = {
      @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
      @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")})
public class SalutationBean implements MessageListener {

  @Override
  public void onMessage(Message message) {

    String name = null;

    try {
      name = message.getStringProperty("name");
    } catch (JMSException ex) {
      System.err.println("Error while retrieving property from message: " + ex);
    }

    System.out.println(String.format("The salutation is: Dear %s", name));
  }
}
