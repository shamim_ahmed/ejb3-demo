package edu.buet.cse.ch01.v14.ejb;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.embeddable.EJBContainer;
import junit.framework.Assert;

import org.junit.Test;

public class TipOfTheDayTest {

  @Test
  public void testGetTip() throws Exception {
    Map<String, Object> props = new HashMap<>();
    props.put(EJBContainer.APP_NAME, "javaee-ch01-v14");
    EJBContainer container = EJBContainer.createEJBContainer(props);

    TipOfTheDayRemote tipOfTheDay = (TipOfTheDayRemote) container.getContext().lookup("java:global/javaee-ch01-v14/classes/TipOfTheDay!edu.buet.cse.ch01.v14.ejb.TipOfTheDayRemote");
    String tip = tipOfTheDay.getNextTip();
    Assert.assertNotNull(tip);
    Assert.assertEquals("In God we trust, all others pay cash", tip);
  }
}
