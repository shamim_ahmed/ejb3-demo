package edu.buet.cse.ch01.v14.ejb;

import javax.ejb.Remote;

@Remote
public interface TipOfTheDayRemote {
  String getNextTip();
}
