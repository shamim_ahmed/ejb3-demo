package edu.buet.cse.ch01.v14.ejb;

import javax.ejb.Stateless;
import javax.ejb.EJB;

@Stateless
public class TipOfTheDay implements TipOfTheDayRemote {

  @EJB
  private TipProviderBean tipProvider;

  @Override
  public String getNextTip() {
    return tipProvider.getNextTip();
  }
}
