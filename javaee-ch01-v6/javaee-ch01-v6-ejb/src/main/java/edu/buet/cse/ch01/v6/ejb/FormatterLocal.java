package edu.buet.cse.ch01.v6.ejb;

import javax.ejb.Local;

@Local
public interface FormatterLocal {
  String format(String greeting, String name);
}
