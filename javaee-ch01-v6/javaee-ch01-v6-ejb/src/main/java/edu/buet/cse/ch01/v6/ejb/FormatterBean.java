package edu.buet.cse.ch01.v6.ejb;

import javax.ejb.Local;
import javax.ejb.Stateless;

@Stateless 
@Local(FormatterLocal.class)
@SuppressWarnings("BeanImplementsBI")
public class FormatterBean {
  
  public String format(String greeting, String name) {
    return String.format("%s, %s", greeting, name);
  }
}
