package edu.buet.cse.ch01.v6.ejb;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;
import javax.naming.InitialContext;
import javax.naming.NamingException;

@Stateless
public class SalutationBean implements SalutationLocal, SalutationRemote {

  @PostConstruct
  public void init() {
    System.out.println("SalutationBean initialized");
  }

  @Override
  public String getFormalSalutation(String name) {
    return getFormattedGreeting("Dear", name);
  }

  @Override
  public String getInformalSalutation(String name) {
    return getFormattedGreeting("Hello", name);
  }

  private String getFormattedGreeting(String greeting, String name) {
    String result = "";

    try {
      InitialContext context = new InitialContext();
      FormatterLocal formatter = (FormatterLocal) context.lookup("java:module/FormatterBean!edu.buet.cse.ch01.v6.ejb.FormatterLocal");
      result = formatter.format(greeting, name);
    } catch (NamingException ex) {
      System.err.println(ex);
    }

    return result;
  }
}
