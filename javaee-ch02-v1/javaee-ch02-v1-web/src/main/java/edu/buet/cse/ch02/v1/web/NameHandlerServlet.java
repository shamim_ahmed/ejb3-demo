package edu.buet.cse.ch02.v1.web;

import edu.buet.cse.ch02.v1.ejb.NameProcessorBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.annotation.WebServlet;

import javax.ejb.EJB;
import org.apache.commons.lang3.StringUtils;

@WebServlet(urlPatterns = "/name-list")
public class NameHandlerServlet extends HttpServlet {
  private static final String PARAM_NAME = "customerName";
  
  @EJB
  private NameProcessorBean nameProcessor;

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    List<String> nameList = nameProcessor.getNames();

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    out.println("<html>");
    out.println("<head>");
    out.println("<title>Name List</title>");
    out.println("</head>");
    out.println("<body>");
    out.println("<ul>");

    for (String name : nameList) {
      out.println("<li>" + name + "</li>");
    }

    out.println("</ul>");
    out.println("</body>");
    out.println("</html>");

    out.flush();
  }
  
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String name = request.getParameter(PARAM_NAME);
    String result;
    
    if (StringUtils.isNotBlank(name)) {
      nameProcessor.addName(name);
      result = String.format("Name %s has been saved successfully", name);
    } else {
      result = "No valid name has been provided";
    }
    
    response.setContentType("text/html");
    
    PrintWriter out = response.getWriter();
    out.println("<html>");
    out.println("<head>");
    out.println("<title>Result</title>");
    out.println("</head>");
    out.println("<body>");
    out.println("<p>");
    out.println(result);
    out.println("</p>");
    out.println("</body>");
    out.println("</html>");
    
    out.flush();
  }
}
