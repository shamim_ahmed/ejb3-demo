<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Home Page</title>
  </head>
  <body>
    <h1>Simple example of Stateful Session Bean</h1>
    
    <form action="${pageContext.servletContext.contextPath}/name-list" method="POST">
      <input type="text" name="customerName" />
      <input type="submit" value="Sumit"/>
    </form>
  </body>
</html>
