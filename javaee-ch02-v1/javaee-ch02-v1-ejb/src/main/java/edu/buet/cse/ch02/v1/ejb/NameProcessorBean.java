package edu.buet.cse.ch02.v1.ejb;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.LocalBean;
import javax.ejb.Stateful;

@Stateful
@LocalBean
public class NameProcessorBean {

  private List<String> nameList;

  @PostConstruct
  public void init() {
    nameList = new ArrayList<>();
  }

  public void addName(String name) {
    nameList.add(name);
  }

  public List<String> getNames() {
    return Collections.unmodifiableList(nameList);
  }

  @PreDestroy
  public void cleanUp() {
    System.out.println("NameProcessorBean is being removed...");
  }
}
