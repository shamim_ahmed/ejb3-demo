package edu.buet.cse.ch02.v5.util;

public enum State {
  PAUSED, RUNNING, TERMINATED
}
