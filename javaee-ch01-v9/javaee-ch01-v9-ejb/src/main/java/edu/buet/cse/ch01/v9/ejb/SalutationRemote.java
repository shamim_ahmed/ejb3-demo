package edu.buet.cse.ch01.v9.ejb;

public interface SalutationRemote {
  String getFormalSalutation(String name);
}
