package edu.buet.cse.ch01.v9.ejb;

public interface SalutationLocal {
  String getInformalSalutation(String name);
}
