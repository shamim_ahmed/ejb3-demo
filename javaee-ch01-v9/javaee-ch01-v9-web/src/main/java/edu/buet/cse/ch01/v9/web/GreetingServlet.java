package edu.buet.cse.ch01.v9.web;

import edu.buet.cse.ch01.v9.ejb.SalutationLocal;
import edu.buet.cse.ch01.v9.ejb.SalutationRemote;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.annotation.WebServlet;
import org.apache.commons.lang3.StringUtils;

import javax.ejb.EJB;

@WebServlet(urlPatterns = "/greeting")
public class GreetingServlet extends HttpServlet {

  private static final String NAME_PARAM_NAME = "name";
  private static final String DEFAULT_NAME = "Duke";

  @EJB
  private SalutationLocal salutationLocal;

  @EJB
  private SalutationRemote salutationRemote;

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    response.setContentType("text/html");

    String name = request.getParameter(NAME_PARAM_NAME);

    if (StringUtils.isBlank(name)) {
      name = DEFAULT_NAME;
    }

    String informalGreeting = salutationLocal.getInformalSalutation(name);
    String formalGreeting = salutationRemote.getFormalSalutation(name);

    PrintWriter out = response.getWriter();
    out.println("<html>");
    out.println("<head>");
    out.println("<title>Salutation Service</title>");
    out.println("</head>");
    out.println("<body>");
    out.println(String.format("<p>Informal Greeting: %s</p>", informalGreeting));
    out.println(String.format("<p>Formal Greeting: %s</p>", formalGreeting));
    out.println("</body>");
    out.println("</html>");

    out.flush();
  }
}
