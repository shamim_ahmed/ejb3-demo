package edu.buet.cse.ch01.v19.ejb;

import javax.ejb.Remote;

@Remote
public interface TipProviderRemote {
  String getTipRemote();
}
