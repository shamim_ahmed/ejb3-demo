package edu.buet.cse.ch01.v19.ejb;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.jws.WebService;

@Stateless
@WebService
public class TipOfTheDay {

  @Resource
  private SessionContext sessionContext;

  public String getNextTip() {
    String result = null;
    TipProviderRemote provider = (TipProviderRemote) sessionContext.lookup("java:global/javaee-ch01-v19/TipProviderBean!edu.buet.cse.ch01.v19.ejb.TipProviderRemote");

    if (provider != null) {
      result = provider.getTipRemote();
    }

    return result;
  }
}
