package edu.buet.cse.ch01.v19.ejb;

import javax.ejb.Local;

@Local
public interface TipProviderLocal {
  String getTipLocal();
}
