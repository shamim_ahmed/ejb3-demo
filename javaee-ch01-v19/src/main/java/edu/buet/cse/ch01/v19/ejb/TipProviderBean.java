package edu.buet.cse.ch01.v19.ejb;

import javax.ejb.Stateless;
import edu.buet.cse.ch01.v19.ejb.interceptor.UpperCaseInterceptor;
import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.interceptor.Interceptors;

@Stateless
public class TipProviderBean implements TipProviderLocal, TipProviderRemote {

  private static final String TIP = "In God we trust, all others pay cash";

  @Resource
  private SessionContext sessionContext;

  @Interceptors(UpperCaseInterceptor.class)
  @Override
  public String getTipLocal() {
    return TIP;
  }

  @Override
  public String getTipRemote() {
    TipProviderLocal provider = sessionContext.getBusinessObject(TipProviderLocal.class);
    return provider.getTipLocal();
  }
}
