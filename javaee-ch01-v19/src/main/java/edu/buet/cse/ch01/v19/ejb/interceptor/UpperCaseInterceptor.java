package edu.buet.cse.ch01.v19.ejb.interceptor;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

@Interceptor
public class UpperCaseInterceptor {

  @AroundInvoke
  public Object interceptInvocation(InvocationContext invocationCtx) throws Exception {
    String result = (String) invocationCtx.proceed();

    if (result != null) {
      result = result.toUpperCase();
    }

    return result;
  }
}
