package edu.buet.cse.ch02.v11.ejb;

import javax.ejb.Remote;

@Remote
public interface RateCalculatorRemote {

  void calculateNewRate(InterestRate rate);
}
