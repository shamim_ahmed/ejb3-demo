package edu.buet.cse.ch02.v11.ejb;

import java.math.BigDecimal;
import javax.ejb.Stateless;

@Stateless
public class RateCalculatorBean implements RateCalculatorRemote {

  @Override
  public void calculateNewRate(InterestRate rate) {
    rate.setValue(BigDecimal.valueOf(2.5));

    System.out.println("interest rate was updated successfully...");
  }
}
