package edu.buet.cse.ch02.v6.util;

public enum State {
  PAUSED, RUNNING, TERMINATED
}
