package edu.buet.cse.ch02.v6.ejb;

import edu.buet.cse.ch02.v6.util.State;
import java.util.concurrent.TimeUnit;
import javax.annotation.PostConstruct;
import javax.ejb.AccessTimeout;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;

@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
public class StateContainerBean {
  private State state;
  
  @PostConstruct
  private void init() {
    state = State.PAUSED;
  }

  @Lock(LockType.READ)
  public State getState() {
    return state;
  }

  @Lock(LockType.WRITE)
  @AccessTimeout(value = 60, unit = TimeUnit.SECONDS)
  public void setState(State state) {
    this.state = state;
  }
}
