package edu.buet.cse.ch01.v3.ejb;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;

@Stateless
public class SalutationBean implements SalutationLocal, SalutationRemote {

  @PostConstruct
  public void init() {
    System.out.println("SalutationBean initialized");
  }

  @Override
  public String getFormalSalutation(String name) {
    return String.format("Dear %s", name);
  }

  @Override
  public String getInformalSalutation(String name) {
    return String.format("Hi %s", name);
  }
}
