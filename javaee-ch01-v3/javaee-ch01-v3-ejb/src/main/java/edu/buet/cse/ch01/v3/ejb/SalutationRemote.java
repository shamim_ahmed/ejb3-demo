package edu.buet.cse.ch01.v3.ejb;

import javax.ejb.Remote;

@Remote
public interface SalutationRemote {
  String getFormalSalutation(String name);
}
