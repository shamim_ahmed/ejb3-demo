package edu.buet.cse.ch02.v12.web;

import edu.buet.cse.ch02.v12.ejb.AccountBean;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.annotation.WebServlet;

import javax.ejb.EJB;

@WebServlet(urlPatterns = "/account-info")
public class AccountInfoServlet extends HttpServlet {

  @EJB(lookup = "java:app/javaee-ch02-v12-ejb/AccountBean")
  private AccountBean accountBean;

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    out.println("<html>");
    out.println("<head>");
    out.println("<title>Account Info</title>");
    out.println("</head>");
    out.println("<body>");
    out.println("<p>");
    out.println(String.format("Balance: %s", accountBean.getBalance()));
    out.println("</p>");
    out.println("</body>");
    out.println("</html>");

    out.flush();
  }
}
