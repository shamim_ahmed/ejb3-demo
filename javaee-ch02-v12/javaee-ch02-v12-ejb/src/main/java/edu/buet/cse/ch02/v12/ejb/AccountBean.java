package edu.buet.cse.ch02.v12.ejb;

import java.math.BigDecimal;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Stateful;
import javax.ejb.Remove;

@Stateful
public class AccountBean {

  private BigDecimal balance;

  public BigDecimal getBalance() {
    return balance;
  }

  public void setBalance(BigDecimal balance) {
    this.balance = balance;
  }

  @PostConstruct
  public void init() {
    balance = BigDecimal.valueOf(100.0);
  }

  @Remove
  public void remove() {
    System.out.println("remove() has been invoked...");
  }

  @PreDestroy
  public void cleanup() {
    System.out.println("AccountBean is being destroyed...");
  }
}
