package edu.buet.cse.ch01.v8.ejb;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Singleton;

@Singleton
@Local(SalutationLocal.class)
@Remote(SalutationRemote.class)
public class SalutationBean implements SalutationLocal, SalutationRemote {

  @PostConstruct
  public void init() {
    System.out.println("SalutationBean initialized");
  }

  @Override
  public String getFormalSalutation(String name) {
    return String.format("Dear %s", name);
  }

  @Override
  public String getInformalSalutation(String name) {
    return String.format("Hi %s", name);
  }
  
  @PreDestroy
  public void cleanup() {
    System.out.println("SalutationBean is being destroyed...");
  }
}
