package edu.buet.cse.ch01.v8.ejb;

public interface SalutationLocal {
  String getInformalSalutation(String name);
}
