package edu.buet.cse.ch01.v8.ejb;

public interface SalutationRemote {
  String getFormalSalutation(String name);
}
