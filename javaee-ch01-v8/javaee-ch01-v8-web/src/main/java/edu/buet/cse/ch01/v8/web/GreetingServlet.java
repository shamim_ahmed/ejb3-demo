package edu.buet.cse.ch01.v8.web;

import edu.buet.cse.ch01.v8.ejb.SalutationLocal;
import edu.buet.cse.ch01.v8.ejb.SalutationRemote;
import java.io.IOException;
import java.io.PrintWriter;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.annotation.WebServlet;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author shamim
 */
@WebServlet(urlPatterns = "/greeting")
public class GreetingServlet extends HttpServlet {

  private static final String NAME_PARAM_NAME = "name";
  private static final String DEFAULT_NAME = "Duke";

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    response.setContentType("text/html");

    String name = request.getParameter(NAME_PARAM_NAME);

    if (StringUtils.isBlank(name)) {
      name = DEFAULT_NAME;
    }

    String formalGreeting = "";
    String informalGreeting = "";

    try {
      InitialContext initialContext = new InitialContext();
      SalutationRemote salutationRemote = (SalutationRemote) initialContext.lookup("java:global/javaee-ch01-v8/javaee-ch01-v8-ejb/SalutationBean!edu.buet.cse.ch01.v8.ejb.SalutationRemote");
      formalGreeting = salutationRemote.getFormalSalutation(name);

      SalutationLocal salutationLocal = (SalutationLocal) initialContext.lookup("java:global/javaee-ch01-v8/javaee-ch01-v8-ejb/SalutationBean!edu.buet.cse.ch01.v8.ejb.SalutationLocal");
      informalGreeting = salutationLocal.getInformalSalutation(name);
    } catch (NamingException ex) {
      System.err.println(ex);
    }

    PrintWriter out = response.getWriter();
    out.println("<html>");
    out.println("<head>");
    out.println("<title>Salutation Service</title>");
    out.println("</head>");
    out.println("<body>");
    out.println(String.format("<p>Formal Greeting: %s</p>", formalGreeting));
    out.println(String.format("<p>Informal Greeting: %s</p>", informalGreeting));
    out.println("</body>");
    out.println("</html>");

    out.flush();
  }
}
