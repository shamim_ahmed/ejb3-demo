package edu.buet.cse.ch03.v1.web;

import java.io.IOException;
import java.io.PrintWriter;
import javax.annotation.Resource;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/send-message")
public class MessageSenderServlet extends HttpServlet {

  @Resource(mappedName = "jms/ch03_connectionFactory")
  private QueueConnectionFactory connectionFactory;

  @Resource(mappedName = "jms/ch03_queue")
  private Queue queue;

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    boolean result = false;

    try {
      QueueConnection connection = connectionFactory.createQueueConnection();
      Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
      MessageProducer producer = session.createProducer(queue);

      TextMessage message = session.createTextMessage();
      message.setText("In God we trust, all others pay cash");
      producer.send(message);

      result = true;
    } catch (Exception ex) {
      System.err.println(ex);
    }

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();

    out.println("<html>");
    out.println("<head>");
    out.println("<title>Result from PrinterBean</title>");
    out.println("</head>");
    out.println("<body>");
    out.println("<p>");

    if (result) {
      out.println("Message was sent successfully");
    } else {
      out.println("Message was not sent");
    }
    
    out.println("</p>");
    out.println("</body>");
    out.println("</html>");

    out.flush();
  }
}
