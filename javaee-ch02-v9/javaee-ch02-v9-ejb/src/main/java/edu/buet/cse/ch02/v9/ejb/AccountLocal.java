package edu.buet.cse.ch02.v9.ejb;

import java.math.BigDecimal;
import javax.ejb.Local;

@Local
public interface AccountLocal {

  BigDecimal getCorporateDiscount();

  void setCorporateDiscount(BigDecimal corporateDiscount);

  BigDecimal getNonProfitDiscount();

  void setNonProfitDiscount(BigDecimal nonProfitDiscount);
}
