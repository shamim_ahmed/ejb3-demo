package edu.buet.cse.ch02.v9.ejb;

import java.math.BigDecimal;
import javax.annotation.PostConstruct;
import javax.ejb.Stateless;

@Stateless
@SuppressWarnings("BMnotPartOfRBIandLBI")
public class AccountBean implements AccountLocal, AccountRemote {

  private BigDecimal corporateDiscount;
  private BigDecimal nonProfitDiscount;
  
  @PostConstruct
  private void init() {
    corporateDiscount = BigDecimal.valueOf(0.2);
    nonProfitDiscount = BigDecimal.valueOf(0.5);
  }

  @Override
  public BigDecimal getCorporateDiscount() {
    return corporateDiscount;
  }

  @Override
  public void setCorporateDiscount(BigDecimal corporateDiscount) {
    this.corporateDiscount = corporateDiscount;
  }

  @Override
  public BigDecimal getNonProfitDiscount() {
    return nonProfitDiscount;
  }

  @Override
  public void setNonProfitDiscount(BigDecimal nonProfitDiscount) {
    this.nonProfitDiscount = nonProfitDiscount;
  }
}
