package edu.buet.cse.ch02.v9.ejb;

import java.math.BigDecimal;
import javax.ejb.Remote;

@Remote
public interface AccountRemote {

  BigDecimal getCorporateDiscount();

  void setCorporateDiscount(BigDecimal corporateDiscount);
}
