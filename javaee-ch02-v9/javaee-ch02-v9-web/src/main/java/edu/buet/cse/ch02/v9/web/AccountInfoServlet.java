package edu.buet.cse.ch02.v9.web;

import edu.buet.cse.ch02.v9.ejb.AccountLocal;
import edu.buet.cse.ch02.v9.ejb.AccountRemote;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.annotation.WebServlet;

import javax.ejb.EJB;

@WebServlet(urlPatterns = "/account-info")
public class AccountInfoServlet extends HttpServlet {

  @EJB
  private AccountLocal accountLocal;

  @EJB
  private AccountRemote accountRemote;

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    out.println("<html>");
    out.println("<head>");
    out.println("<title>Account Info</title>");
    out.println("</head>");
    out.println("<body>");
    out.println("<p>");
    out.println(String.format("Corporate discount: %s", accountRemote.getCorporateDiscount()));
    out.println("<br/>");
    out.println(String.format("Non-profit discount: %s", accountLocal.getNonProfitDiscount()));
    out.println("</p>");
    out.println("</body>");
    out.println("</html>");

    out.flush();
  }
}
