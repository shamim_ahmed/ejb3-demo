package edu.buet.cse.ch01.v20.ejb;

import javax.ejb.LocalBean;
import javax.ejb.SessionContext;
import javax.ejb.Singleton;
import javax.naming.InitialContext;
import javax.naming.NamingException;

@Singleton
@LocalBean
public class ContextInfoProviderBean {

  public String getContextInfo() {
    StringBuilder resultBuilder = new StringBuilder();

    try {
      InitialContext initialCtx = new InitialContext();
      SessionContext sessionCtx = (SessionContext) initialCtx.lookup("java:comp/EJBContext");
      resultBuilder.append(String.format("Caller principal: %s%n", sessionCtx.getCallerPrincipal()));
      resultBuilder.append(String.format("Invoked business interface : %s%n", sessionCtx.getInvokedBusinessInterface().getCanonicalName()));
    } catch (NamingException ex) {
      System.err.println(ex);
    }

    return resultBuilder.toString();
  }
}
