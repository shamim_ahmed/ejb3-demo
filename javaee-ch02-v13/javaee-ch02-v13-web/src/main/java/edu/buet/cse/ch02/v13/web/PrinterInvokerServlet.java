package edu.buet.cse.ch02.v13.web;

import edu.buet.cse.ch02.v13.ejb.PrinterBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.annotation.WebServlet;

import javax.ejb.EJB;

@WebServlet(urlPatterns = "/invoke-printer")
public class PrinterInvokerServlet extends HttpServlet {

  @EJB(lookup = "java:app/javaee-ch02-v13-ejb/PrinterBean!edu.buet.cse.ch02.v13.ejb.PrinterBean")
  private PrinterBean printerBean;

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {

    printerBean.invokeAndContinue();

    Future<String> result = printerBean.invokeAndWait();
    String outputStr = null;

    try {
      outputStr = result.get();
    } catch (InterruptedException | ExecutionException ex) {
      System.err.println(ex);
    }

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();

    out.println("<html>");
    out.println("<head>");
    out.println("<title>Result from PrinterBean</title>");
    out.println("</head>");
    out.println("<body>");
    out.println("<p>");
    out.println(String.format("The output is: %s", outputStr));
    out.println("</p>");
    out.println("</body>");
    out.println("</html>");

    out.flush();
  }
}
