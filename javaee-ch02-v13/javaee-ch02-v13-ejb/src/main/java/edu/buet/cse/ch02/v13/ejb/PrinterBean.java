package edu.buet.cse.ch02.v13.ejb;

import java.util.concurrent.Future;
import javax.ejb.Stateless;
import javax.ejb.Asynchronous;
import javax.ejb.AsyncResult;

@Stateless
public class PrinterBean {

  @Asynchronous
  public void invokeAndContinue() {
    System.out.println("invokeAndContinue() has been called...");
  }

  @Asynchronous
  public Future<String> invokeAndWait() {
    return new AsyncResult<>("In God we trust, all others pay cash");
  }
}
