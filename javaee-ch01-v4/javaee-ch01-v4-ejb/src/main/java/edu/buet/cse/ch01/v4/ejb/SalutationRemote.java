package edu.buet.cse.ch01.v4.ejb;

public interface SalutationRemote {
  String getFormalSalutation(String name);
}
