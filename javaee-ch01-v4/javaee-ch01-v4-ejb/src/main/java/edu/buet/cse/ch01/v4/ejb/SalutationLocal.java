package edu.buet.cse.ch01.v4.ejb;

public interface SalutationLocal {
  String getInformalSalutation(String name);
}
