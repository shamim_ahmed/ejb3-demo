package edu.buet.cse.ch02.v3.ejb;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;

@Singleton
@LocalBean
public class PlayerBean {

  private String name;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @PostConstruct
  private void init() {
    name = "Warrior Monk";
    System.out.printf("PlayerBean has been initialized with name %s%n", name);
  }
}
