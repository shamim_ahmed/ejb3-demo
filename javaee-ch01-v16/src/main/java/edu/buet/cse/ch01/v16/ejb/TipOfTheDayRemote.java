package edu.buet.cse.ch01.v16.ejb;

import javax.ejb.Remote;

@Remote
public interface TipOfTheDayRemote {
  String getNextTip();
}
