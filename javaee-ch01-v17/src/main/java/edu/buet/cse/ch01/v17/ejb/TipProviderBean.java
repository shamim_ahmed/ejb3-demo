package edu.buet.cse.ch01.v17.ejb;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Stateless;
import edu.buet.cse.ch01.v17.ejb.interceptor.UpperCaseInterceptor;
import javax.interceptor.Interceptors;

@Stateless(name = "ejb/MyCustomTipProvider")
public class TipProviderBean {

  private static final String TIP = "In God we trust, all others pay cash";
  
  @PostConstruct
  public void init() {
    System.out.println("TipProviderBean has been initialized...");
  }

  @Interceptors(UpperCaseInterceptor.class)
  public String getNextTip() {
    return TIP;
  }
  
  @PreDestroy
  public void cleanUp() {
    System.out.println("TipProviderBean is being destroyed...");
  }
}
