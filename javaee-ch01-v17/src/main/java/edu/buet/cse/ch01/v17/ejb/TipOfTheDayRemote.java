package edu.buet.cse.ch01.v17.ejb;

import javax.ejb.Remote;

@Remote
public interface TipOfTheDayRemote {
  String getNextTip();
}
