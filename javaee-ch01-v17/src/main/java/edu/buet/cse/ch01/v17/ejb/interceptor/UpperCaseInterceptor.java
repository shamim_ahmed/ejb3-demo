package edu.buet.cse.ch01.v17.ejb.interceptor;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

public class UpperCaseInterceptor {

  @AroundInvoke
  public Object interceptInvocation(InvocationContext invocationContext) throws Exception {
    String result = (String) invocationContext.proceed();

    if (result != null) {
      result = result.toUpperCase();
    }

    return result;
  }
}
