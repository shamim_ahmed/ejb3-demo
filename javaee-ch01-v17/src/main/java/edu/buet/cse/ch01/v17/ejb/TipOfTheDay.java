package edu.buet.cse.ch01.v17.ejb;

import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.naming.InitialContext;
import javax.naming.NamingException;

@Stateless
@WebService
public class TipOfTheDay implements TipOfTheDayRemote {

  @Override
  public String getNextTip() {
    String result = null;

    try {
      InitialContext initialContext = new InitialContext();
      TipProviderBean tipProvider = (TipProviderBean) initialContext.lookup("java:module/ejb/MyCustomTipProvider");
      result = tipProvider.getNextTip();
    } catch (NamingException ex) {
      System.err.println(ex);
    }
    
    return result;
  }
}
