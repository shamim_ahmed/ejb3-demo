package edu.buet.cse.ch02.v7.ejb;

import edu.buet.cse.ch02.v7.util.State;
import javax.annotation.PostConstruct;
import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Singleton;

@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
public class StateContainerBean {

  private State state;

  @PostConstruct
  private void init() {
    state = State.PAUSED;
  }

  public synchronized State getState() {
    return state;
  }

  public synchronized void setState(State state) {
    this.state = state;
  }
}
