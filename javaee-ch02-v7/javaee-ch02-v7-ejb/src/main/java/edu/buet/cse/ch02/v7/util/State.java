package edu.buet.cse.ch02.v7.util;

public enum State {
  PAUSED, RUNNING, TERMINATED
}
