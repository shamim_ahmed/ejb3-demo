package edu.buet.cse.ch01.v5.ejb;

import javax.ejb.Local;

@Local
public interface SalutationLocal {
  String getInformalSalutation(String name);
}
