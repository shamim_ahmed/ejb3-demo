package edu.buet.cse.ch01.v18.ejb;

import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.SessionContext;
import javax.ejb.Singleton;

@Singleton
@LocalBean
public class ContextInfoProvider {

  @Resource
  private SessionContext sessionContext;

  public String getContextInfo() {
    StringBuilder resultBuilder = new StringBuilder();
    resultBuilder.append(String.format("Caller principal: %s%n", sessionContext.getCallerPrincipal()));
    resultBuilder.append(String.format("Rollback only : %b%n", sessionContext.getRollbackOnly()));
    
    return resultBuilder.toString();
  }
}
