package edu.buet.cse.ch01.v18.web;

import edu.buet.cse.ch01.v18.ejb.ContextInfoProvider;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.annotation.WebServlet;

import javax.ejb.EJB;

@WebServlet(urlPatterns = "/context-info")
public class ContextInfoServlet extends HttpServlet {

  @EJB
  private ContextInfoProvider infoProvider;

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    response.setContentType("text/plain");

    PrintWriter out = response.getWriter();
    out.println(infoProvider.getContextInfo());
    out.flush();
  }
}
