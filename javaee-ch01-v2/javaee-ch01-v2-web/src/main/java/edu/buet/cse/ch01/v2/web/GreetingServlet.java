package edu.buet.cse.ch01.v2.web;

import edu.buet.cse.ch01.v2.ejb.SalutationBean;
import java.io.IOException;
import java.io.PrintWriter;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.annotation.WebServlet;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author shamim
 */
@WebServlet(urlPatterns = "/greeting")
public class GreetingServlet extends HttpServlet {

  private static final String NAME_PARAM_NAME = "name";
  private static final String DEFAULT_NAME = "Duke";

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    response.setContentType("text/html");

    String name = request.getParameter(NAME_PARAM_NAME);

    if (StringUtils.isBlank(name)) {
      name = DEFAULT_NAME;
    }

    String result = "";

    try {
      InitialContext initialContext = new InitialContext();
      SalutationBean salutationBean = (SalutationBean) initialContext.lookup("java:global/javaee-ch01-v2/javaee-ch01-v2-ejb/SalutationBean");
      result = salutationBean.getFormalSalutation(name);
    } catch (NamingException ex) {
      System.err.println(ex);
    }

    PrintWriter out = response.getWriter();
    out.println("<html>");
    out.println("<head>");
    out.println("<title>Salutation Service</title>");
    out.println("</head>");
    out.println("<body>");
    out.println(String.format("<p>%s</p>", result));
    out.println("</body>");
    out.println("</html>");

    out.flush();
  }
}
