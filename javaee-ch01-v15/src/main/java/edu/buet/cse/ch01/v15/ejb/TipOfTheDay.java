package edu.buet.cse.ch01.v15.ejb;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.SessionContext;
import javax.jws.WebService;

@Stateless
@WebService
public class TipOfTheDay implements TipOfTheDayRemote {

  @Resource
  private SessionContext sessionContext;
  
  @Override
  public String getNextTip() {
    TipProviderBean tipProvider = (TipProviderBean) sessionContext.lookup("java:global/javaee-ch01-v15/TipProviderBean");
    return tipProvider.getNextTip();
  }
}
