package edu.buet.cse.ch01.v15.ejb;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;

@Singleton
@Startup
public class TipProviderBean {

  private static final String TIP = "In God we trust, all others pay cash";
  
  @PostConstruct
  public void init() {
    System.out.println("TipProviderBean has been initialized...");
  }

  public String getNextTip() {
    return TIP;
  }
}
