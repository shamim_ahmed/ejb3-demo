package edu.buet.cse.ch01.v11.ejb;

public enum OutputType {
  DATE, TIME, DATE_AND_TIME
}
