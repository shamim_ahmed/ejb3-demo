package edu.buet.cse.ch01.v11.ejb;

import javax.jws.WebService;
import javax.ejb.Stateless;
import javax.ejb.EJB;

@Stateless
@WebService
public class DateTimeProvider {
  
  @EJB
  private DateFormatterBean formatterBean;
  
  public String getCurrentDate() {
    return String.format("Current date is %s", formatterBean.getFormattedResult(OutputType.DATE));
  }
  
  public String getCurrentTime() {
    return String.format("Current time is %s", formatterBean.getFormattedResult(OutputType.TIME));
  }
  
  public String getCurrentDateTime() {
    return String.format("Current date and time is %s", formatterBean.getFormattedResult(OutputType.DATE_AND_TIME));
  }
}
