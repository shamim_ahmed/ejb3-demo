package edu.buet.cse.ch01.v11.ejb;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.ejb.Singleton;

@Singleton
public class DateFormatterBean {

  private static final String DATE_FORMAT_STR = "yyyy-MM-dd";
  private static final String TIME_FORMAT_STR = "HH:mm:ss";
  private static final String DATE_TIME_FORMAT_STR = "yyyy-MM-dd HH:mm:ss";

  public String getFormattedResult(OutputType outputType) {
    Calendar calendar = Calendar.getInstance();
    DateFormat dateFormat = null;

    switch (outputType) {
      case DATE:
        dateFormat = new SimpleDateFormat(DATE_FORMAT_STR);
        break;
      case TIME:
        dateFormat = new SimpleDateFormat(TIME_FORMAT_STR);
        break;
      case DATE_AND_TIME:
        dateFormat = new SimpleDateFormat(DATE_TIME_FORMAT_STR);
        break;
      default:
        dateFormat = new SimpleDateFormat(DATE_FORMAT_STR);
        break;
    }

    return dateFormat.format(calendar.getTime());
  }
}
