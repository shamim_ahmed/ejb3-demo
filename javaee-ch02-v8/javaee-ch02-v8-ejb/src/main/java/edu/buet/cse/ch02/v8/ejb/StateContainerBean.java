package edu.buet.cse.ch02.v8.ejb;

import edu.buet.cse.ch02.v8.util.State;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;

@Singleton
@Startup
public class StateContainerBean {

  private State state;

  @PostConstruct
  private void init() {
    state = State.PAUSED;
    System.out.println("Initializing the bean...");
  }

  public State getState() {
    return state;
  }

  public void setState(State state) {
    this.state = state;
  }

  @PreDestroy
  private void cleanup() {
    state = State.TERMINATED;
    System.out.println("Terminating the bean...");
  }
}
