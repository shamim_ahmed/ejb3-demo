package edu.buet.cse.ch02.v8.util;

public enum State {
  PAUSED, RUNNING, TERMINATED
}
