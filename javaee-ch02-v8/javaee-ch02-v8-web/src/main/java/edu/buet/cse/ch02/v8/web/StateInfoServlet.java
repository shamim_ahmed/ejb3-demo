package edu.buet.cse.ch02.v8.web;

import edu.buet.cse.ch02.v8.ejb.StateContainerBean;
import edu.buet.cse.ch02.v8.util.State;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.annotation.WebServlet;

import javax.ejb.EJB;

@WebServlet(urlPatterns = "/state-info")
public class StateInfoServlet extends HttpServlet {

  private static final String PARAM_NAME = "state";

  @EJB
  private StateContainerBean stateContainer;

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    out.println("<html>");
    out.println("<head>");
    out.println("<title>State Info</title>");
    out.println("</head>");
    out.println("<body>");
    out.println("<p>");
    out.println(String.format("Current State: %s", stateContainer.getState()));
    out.println("</p>");
    out.println("</body>");
    out.println("</html>");

    out.flush();
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    String stateStr = request.getParameter(PARAM_NAME);
    State nextState = null;

    if (stateStr == null) {
      nextState = State.PAUSED;
    } else if (stateStr.equalsIgnoreCase("paused")) {
      nextState = State.PAUSED;
    } else if (stateStr.equalsIgnoreCase("running")) {
      nextState = State.RUNNING;
    } else if (stateStr.equalsIgnoreCase("terminated")) {
      nextState = State.TERMINATED;
    } else {
      nextState = State.PAUSED;
    }

    stateContainer.setState(nextState);

    response.setContentType("text/html");

    PrintWriter out = response.getWriter();
    out.println("<html>");
    out.println("<head>");
    out.println("<title>Result</title>");
    out.println("</head>");
    out.println("<body>");
    out.println("<p>");
    out.println("State was updated successfully");
    out.println("</p>");
    out.println("</body>");
    out.println("</html>");

    out.flush();
  }
}
