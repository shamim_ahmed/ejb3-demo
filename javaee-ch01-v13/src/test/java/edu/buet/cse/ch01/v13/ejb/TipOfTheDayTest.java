package edu.buet.cse.ch01.v13.ejb;

import org.junit.Test;

import javax.ejb.embeddable.EJBContainer;
import junit.framework.Assert;

public class TipOfTheDayTest {
  @Test
  public void testGetTip() throws Exception {
    EJBContainer container = EJBContainer.createEJBContainer();
    TipOfTheDay tipOfTheDay = (TipOfTheDay) container.getContext().lookup("java:global/classes/TipOfTheDay");
    String tip = tipOfTheDay.getNextTip();
    Assert.assertNotNull(tip);
    Assert.assertEquals("In God we trust, all others pay cash", tip);
  }
}
