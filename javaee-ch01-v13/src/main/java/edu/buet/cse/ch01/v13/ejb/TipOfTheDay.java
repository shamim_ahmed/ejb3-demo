package edu.buet.cse.ch01.v13.ejb;

import javax.ejb.Stateless;
import javax.ejb.EJB;
import javax.ejb.LocalBean;

@Stateless
@LocalBean
public class TipOfTheDay {

  @EJB
  private TipProviderBean tipProvider;

  public String getNextTip() {
    return tipProvider.getNextTip();
  }
}
