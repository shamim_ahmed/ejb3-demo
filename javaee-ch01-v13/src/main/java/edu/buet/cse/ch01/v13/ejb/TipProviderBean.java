package edu.buet.cse.ch01.v13.ejb;

import javax.ejb.Singleton;

@Singleton
public class TipProviderBean {

  private static final String TIP = "In God we trust, all others pay cash";

  public String getNextTip() {
    return TIP;
  }
}
