package edu.buet.cse.ch01.v1.ejb;

import javax.annotation.PostConstruct;
import javax.ejb.Stateless;

/**
 *
 * @author shamim
 */
@Stateless
public class SalutationBean {

  @PostConstruct
  public void init() {
    System.out.println("SalutationBean initialized");
  }

  public String getFormalSalutation(String name) {
    return String.format("Dear %s", name);
  }

  public String getInformalSalutation(String name) {
    return String.format("Hi %s", name);
  }
}
