package edu.buet.cse.ch01.v12.ejb;

import javax.jws.WebService;
import javax.ejb.Stateless;
import javax.ejb.EJB;
import javax.jws.WebMethod;

@Stateless
@WebService
public class DateTimeProvider {

  @EJB
  private DateFormatterBean formatterBean;

  @WebMethod(operationName = "currentDate")
  public String getCurrentDate() {
    return String.format("Current date is %s", formatterBean.getFormattedResult(OutputType.DATE));
  }

  @WebMethod(operationName = "currentTime")
  public String getCurrentTime() {
    return String.format("Current time is %s", formatterBean.getFormattedResult(OutputType.TIME));
  }

  @WebMethod(exclude = true)
  public String getCurrentDateTime() {
    return String.format("Current date and time is %s", formatterBean.getFormattedResult(OutputType.DATE_AND_TIME));
  }
}
