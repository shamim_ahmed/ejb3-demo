package edu.buet.cse.ch01.v12.ejb;

public enum OutputType {
  DATE, TIME, DATE_AND_TIME
}
